package com.ubo.tp.twitub.ihm;

import com.ubo.tp.twitub.core.Twitub;
import com.ubo.tp.twitub.datamodel.IDatabaseObserver;
import com.ubo.tp.twitub.datamodel.Twit;
import com.ubo.tp.twitub.datamodel.User;

import javax.swing.*;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Classe de la vue principale de l'application.
 */
public class TwitubMainView implements IDatabaseObserver {

    protected JFrame mainFrame;

    protected final Twitub twitub;

    private final String MAIN_LOGO_PATH = "src/main/resources/images/logo_20.jpg";

    private final String QUIT_LOGO_PATH = "src/main/resources/images/exitIcon_20.png";

    private final String PROPOS_LOGO_PATH = "src/main/resources/images/logo_50.jpg";

    private final String PROPOS = "<html><center>UBO M2-TIIL<br>Département Informatique</center></html>";

    public TwitubMainView(Twitub twitub) {
        mainFrame = new JFrame();
        this.twitub = twitub;

        initGUI();
    }

    protected void initGUI() {
        mainFrame.setTitle("Accueil");
        mainFrame.setSize(1000, 600);
        mainFrame.setIconImage(new ImageIcon(MAIN_LOGO_PATH).getImage());

        initMenuBar();
    }

    protected void initMenuBar() {
        JMenuBar mb = new JMenuBar();

        mb.setSize(10, 10);

        JMenu fichier = new JMenu("Fichier");

        JMenuItem quitter = new JMenuItem("Quitter");
        quitter.setIcon(new ImageIcon(QUIT_LOGO_PATH));
        quitter.addActionListener(e -> mainFrame.dispose());

        fichier.add(quitter);

        mb.add(fichier);

        JMenu aide = new JMenu("?");
        JMenuItem propos = new JMenuItem("A propos");
        propos.addActionListener(e -> JOptionPane.showMessageDialog(mainFrame, PROPOS, "A propos", JOptionPane.INFORMATION_MESSAGE, new ImageIcon(PROPOS_LOGO_PATH)));

        aide.add(propos);

        mb.add(aide);

        mainFrame.setJMenuBar(mb);
    }

    public void initNavTwit(User user) {
        JMenuBar mb = mainFrame.getJMenuBar();

        JMenu twiter = new JMenu("Twiter");

        JMenuItem profile = new JMenuItem("Profil");
        profile.addActionListener(e -> twitub.showProfile(user));

        twiter.add(profile);

        JMenuItem tweeter = new JMenuItem("Tweeter");
        tweeter.addActionListener(e -> twitub.showTweeter());

        twiter.add(tweeter);

        JMenuItem rechercherTwit = new JMenuItem("Rechercher Twit");
        rechercherTwit.addActionListener(e -> twitub.showRechercherTwit(new HashSet<>()));

        twiter.add(rechercherTwit);


        JMenuItem rechercherUser = new JMenuItem("Rechercher User");
        rechercherUser.addActionListener(e -> twitub.showRechercherUser(new ArrayList<>()));

        twiter.add(rechercherUser);

        JMenuItem deconnexion = new JMenuItem("Déconnexion");
        deconnexion.addActionListener(e -> {
            mb.remove(twiter);
            twitub.deconnexion();
        });

        twiter.add(deconnexion);

        mb.add(twiter);

        mainFrame.revalidate();
        mainFrame.repaint();
    }

    public File getRepertoire() {
        JFileChooser selecteurFichiers = new JFileChooser();
        selecteurFichiers.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        selecteurFichiers.showOpenDialog(null);
        return selecteurFichiers.getSelectedFile();
    }

    public void showLogin(LoginView loginView) {
        mainFrame.getContentPane().removeAll();
        mainFrame.getContentPane().add(loginView);

        render();
    }

    public void showCreate(CreateView createView) {
        mainFrame.getContentPane().removeAll();
        mainFrame.getContentPane().add(createView);

        render();
    }

    public void showProfile(ProfileView profileView) {
        mainFrame.getContentPane().removeAll();
        mainFrame.getContentPane().add(profileView);

        render();
    }

    public void showTweeter(TweeterView tweeterView) {
        mainFrame.getContentPane().removeAll();
        mainFrame.getContentPane().add(tweeterView);

        render();
    }

    public void showRechercherTwit(RechercherTwitView rechercherTwitView) {
        mainFrame.getContentPane().removeAll();
        mainFrame.getContentPane().add(rechercherTwitView);

        render();
    }

    public void showRechercherUser(RechercherUserView rechercherUserView) {
        mainFrame.getContentPane().removeAll();
        mainFrame.getContentPane().add(rechercherUserView);

        render();
    }

    public void render() {
        mainFrame.revalidate();
        mainFrame.repaint();
    }

    public void setVisible() {
        mainFrame.setVisible(true);
    }

    @Override
    public void notifyTwitAdded(Twit addedTwit) {

    }

    @Override
    public void notifyTwitDeleted(Twit deletedTwit) {

    }

    @Override
    public void notifyTwitModified(Twit modifiedTwit) {

    }

    @Override
    public void notifyUserAdded(User addedUser) {
    }

    @Override
    public void notifyUserDeleted(User deletedUser) {

    }

    @Override
    public void notifyUserModified(User modifiedUser) {

    }
}
