package com.ubo.tp.twitub.ihm;

import com.ubo.tp.twitub.core.LoginController;

import javax.swing.*;
import java.awt.*;

public class LoginView extends JPanel {

    public LoginView(LoginController loginController, boolean isError) {
        JTextField tag = new JTextField(20);
        tag.setMaximumSize(tag.getPreferredSize());
        JTextField password = new JPasswordField(20);
        password.setMaximumSize(password.getPreferredSize());

        this.add(new JLabel("Tag : "));
        this.add(tag);
        this.add(new JLabel("Mot de passe : "));
        this.add(password);

        if (isError) {
            JLabel error = new JLabel("Identifiants incorrects");
            error.setForeground(Color.RED);
            this.add(error);
        }

        JButton connexion = new JButton("Connexion");
        connexion.addActionListener(e -> loginController.login(tag.getText(), password.getText()));

        JButton creation = new JButton("Créer compte");
        creation.addActionListener(e -> loginController.create());

        this.add(connexion);
        this.add(creation);
    }
}
