package com.ubo.tp.twitub.ihm;

import com.ubo.tp.twitub.core.RechercherUserController;
import com.ubo.tp.twitub.datamodel.User;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class RechercherUserView extends JPanel {

    public RechercherUserView(RechercherUserController rechercherUserController, List<User> users) {
        this.setLayout(new GridBagLayout());

        JPanel panel = new JPanel(new GridBagLayout());

        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.insets = new Insets(10, 10, 10, 10);
        c.anchor = GridBagConstraints.WEST;

        panel.add(new JLabel("User Tag : "));

        JTextField userTag = new JTextField(15);

        panel.add(userTag);

        JButton rechercher = new JButton("Rechercher");
        rechercher.addActionListener(e -> rechercherUserController.rechercherUser(userTag.getText()));

        panel.add(rechercher);

        c.anchor = GridBagConstraints.CENTER;

        c.gridx++;

        users.forEach(user -> {
            c.gridy++;
            panel.add(new UserDetailsView(rechercherUserController, user), c);
        });

        this.add(new JScrollPane(panel), new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
}
