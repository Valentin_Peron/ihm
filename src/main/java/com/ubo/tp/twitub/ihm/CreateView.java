package com.ubo.tp.twitub.ihm;

import com.ubo.tp.twitub.core.CreateController;

import javax.swing.*;
import java.awt.*;

public class CreateView extends JPanel {
    public CreateView(CreateController createController, String erreur) {

        JTextField tag = new JTextField(20);
        tag.setMaximumSize(tag.getPreferredSize());
        JTextField nom = new JTextField(20);
        nom.setMaximumSize(nom.getPreferredSize());
        JTextField password = new JPasswordField(20);
        password.setMaximumSize(password.getPreferredSize());

        this.add(new JLabel("Tag : "));
        this.add(tag);
        this.add(new JLabel("Nom : "));
        this.add(nom);
        this.add(new JLabel("Mot de passe : "));
        this.add(password);

        if (!erreur.equals("")) {
            JLabel error = new JLabel(erreur);
            error.setForeground(Color.RED);
            this.add(error);
        }

        JButton connexion = new JButton("Création");
        connexion.addActionListener(e -> createController.create(tag.getText(), nom.getText(), password.getText()));

        JButton creation = new JButton("Retour");
        creation.addActionListener(e -> createController.retour());

        this.add(connexion);
        this.add(creation);
    }
}
