package com.ubo.tp.twitub.ihm;

import com.ubo.tp.twitub.datamodel.Twit;

import javax.swing.*;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TwitDetailsView extends JPanel {

    public TwitDetailsView(Twit twit) {
        this.setLayout(new BoxLayout(this, 1));
        this.setBorder(BorderFactory.createLineBorder(Color.black));

        JLabel userTag = (new JLabel(twit.getTwiter().getUserTag()));
        this.add(userTag);

        JTextArea twitText = new JTextArea(1, 80);
        twitText.setEditable(false);
        twitText.setLineWrap(true);
        twitText.setText(twit.getText());

        this.add(twitText);

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        JLabel date = (new JLabel(sdf.format(new Date(twit.getEmissionDate()))));
        this.add(date);

        userTag.setAlignmentX(CENTER_ALIGNMENT);
        twitText.setAlignmentX(CENTER_ALIGNMENT);
        date.setAlignmentX(CENTER_ALIGNMENT);
    }
}
