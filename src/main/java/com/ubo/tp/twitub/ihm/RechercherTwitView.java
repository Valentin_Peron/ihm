package com.ubo.tp.twitub.ihm;

import com.ubo.tp.twitub.core.RechercherTwitController;
import com.ubo.tp.twitub.datamodel.Twit;

import javax.swing.*;
import java.awt.*;
import java.util.Set;

public class RechercherTwitView extends JPanel {

    public RechercherTwitView(RechercherTwitController rechercherTwitController, Set<Twit> twits) {
        this.setLayout(new GridBagLayout());

        JPanel panelRechercher = new JPanel();

        panelRechercher.add(new JLabel("Twit : "));

        JTextField keyWord = new JTextField(15);

        panelRechercher.add(keyWord);

        JButton rechercher = new JButton("Rechercher");
        rechercher.addActionListener(e -> rechercherTwitController.rechercherTwit(keyWord.getText()));

        panelRechercher.add(rechercher);

        JPanel panel = new JPanel(new GridBagLayout());

        panel.add(panelRechercher);

        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.insets = new Insets(10, 10, 10, 10);
        c.anchor = GridBagConstraints.CENTER;
        c.weightx = 1;

        twits.forEach(twit -> {
            c.gridy++;
            panel.add(new TwitDetailsView(twit), c);
        });

        this.add(new JScrollPane(panel), new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
}
