package com.ubo.tp.twitub.ihm;

import com.ubo.tp.twitub.core.ProfileController;
import com.ubo.tp.twitub.datamodel.User;

import javax.swing.*;
import java.awt.*;
import java.util.Set;

public class ProfileView extends JPanel {

    public ProfileView(ProfileController profileController, User user, User otherUser, Set<User> follows) {
        this.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.insets = new Insets(10, 10, 10, 10);
        c.anchor = GridBagConstraints.WEST;

        c.gridy = 0;
        JLabel nom = new JLabel("Name : " + otherUser.getName());
        this.add(nom, c);

        c.gridy = 1;
        JLabel tag = new JLabel("Twitter Tag : @" + otherUser.getUserTag());
        this.add(tag, c);

        c.gridy = 2;
        JLabel abonnes = new JLabel("Nombre d'abonnés : " + profileController.getNbFollower(otherUser));
        this.add(abonnes, c);

        c.gridy = 3;
        JLabel abonnements = new JLabel("Nombre d'abonnements : " + otherUser.getFollows().size());
        this.add(abonnements, c);


        if (!user.equals(otherUser)) {
            c.gridy = 4;

            String label = "";

            if (follows.stream().filter(follow -> follow.getUserTag().equals(otherUser.getUserTag())).count() != 0) {
                label = "Unfollow";
            } else {
                label = "Follow";
            }

            JButton follow = new JButton(label);
            follow.addActionListener(e -> profileController.follow(otherUser));
            this.add(follow, c);
        }
    }
}
