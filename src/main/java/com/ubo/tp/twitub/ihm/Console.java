package com.ubo.tp.twitub.ihm;

import com.ubo.tp.twitub.datamodel.IDatabaseObserver;
import com.ubo.tp.twitub.datamodel.Twit;
import com.ubo.tp.twitub.datamodel.User;

import javax.swing.*;
import java.awt.*;

public class Console implements IDatabaseObserver {

    protected JFrame consoleObject;

    protected JPanel panel;

    public Console() {
        initConsole();
    }

    public void initConsole() {
        consoleObject = new JFrame("Console");

        consoleObject.setSize(500, 500);

        panel = new JPanel(new GridLayout(0, 1));

        consoleObject.add(panel);
        consoleObject.setVisible(true);
    }

    @Override
    public void notifyTwitAdded(Twit addedTwit) {
        panel.add(new JLabel("tweet ajouté : " + addedTwit.getText()));
        panel.validate();
        panel.repaint();
    }

    @Override
    public void notifyTwitDeleted(Twit deletedTwit) {
        panel.add(new JLabel("tweet supprimé : " + deletedTwit.getText()));
        panel.validate();
        panel.repaint();
    }

    @Override
    public void notifyTwitModified(Twit modifiedTwit) {
        panel.add(new JLabel("tweet modifié : " + modifiedTwit.getText()));
        panel.validate();
        panel.repaint();
    }

    @Override
    public void notifyUserAdded(User addedUser) {
        panel.add(new JLabel("user ajouté : " + addedUser.getName()));
        panel.validate();
        panel.repaint();
    }

    @Override
    public void notifyUserDeleted(User deletedUser) {
        panel.add(new JLabel("user supprimé : " + deletedUser.getName()));
        panel.validate();
        panel.repaint();
    }

    @Override
    public void notifyUserModified(User modifiedUser) {
        panel.add(new JLabel("user modifié : " + modifiedUser.getName()));
        panel.validate();
        panel.repaint();
    }
}
