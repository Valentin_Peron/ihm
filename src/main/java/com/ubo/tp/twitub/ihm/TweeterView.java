package com.ubo.tp.twitub.ihm;

import com.ubo.tp.twitub.core.TweeterController;

import javax.swing.*;
import java.awt.*;

public class TweeterView extends JPanel {

    public TweeterView(TweeterController tweeterController) {
        this.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.insets = new Insets(10, 10, 10, 10);
        c.anchor = GridBagConstraints.WEST;

        JTextArea textArea = new JTextArea(10, 60);
        textArea.setDocument(new LimitedDocument(250));
        textArea.setLineWrap(true);

        JScrollPane scrollPane = new JScrollPane(textArea);

        c.gridy = 0;
        this.add(scrollPane, c);

        JButton tweeter = new JButton("Tweeter");
        tweeter.addActionListener(e -> tweeterController.tweet(textArea.getText()));

        c.gridy = 1;
        c.anchor = GridBagConstraints.CENTER;
        this.add(tweeter, c);
    }
}
