package com.ubo.tp.twitub.ihm;

import com.ubo.tp.twitub.core.RechercherUserController;
import com.ubo.tp.twitub.datamodel.User;

import javax.swing.*;

public class UserDetailsView extends JPanel {

    public UserDetailsView(RechercherUserController rechercherUserController, User user) {
        this.add(new JLabel(user.getUserTag()));

        JButton consulter = new JButton("Consulter profil");
        consulter.addActionListener(e -> rechercherUserController.consulterProfil(user));

        this.add(consulter);
    }
}
