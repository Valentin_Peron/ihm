package com.ubo.tp.twitub.core;

import com.ubo.tp.twitub.datamodel.IDatabase;
import com.ubo.tp.twitub.datamodel.User;
import com.ubo.tp.twitub.observer.ICreateObserver;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public class CreateController {

    protected final Set<ICreateObserver> observers;

    protected IDatabase database;

    protected EntityManager entityManager;

    public CreateController(IDatabase database, EntityManager entityManager) {
        observers = new HashSet<>();
        this.database = database;
        this.entityManager = entityManager;
    }

    public void addObserver(ICreateObserver observer) {
        observers.add(observer);
    }

    public void removeObserver(ICreateObserver observer) {
        observers.remove(observer);
    }

    public void create(String tag, String nom, String password) {
        if (tag.equals("") || nom.equals("") || password.equals("")) {
            observers.forEach(observer -> observer.notifyNotCreate("Champs obligatoires"));
        } else {
            Set<User> users = database.getUsers();

            Optional<User> tagExistant = users.stream().filter(user -> user.getUserTag().equals(tag)).findFirst();

            if (tagExistant.isPresent()) observers.forEach(observer -> observer.notifyNotCreate("Tag déjà pris"));
            else {
                User newUser = new User(UUID.randomUUID(), tag, password, nom, new HashSet<>(), "");
                entityManager.sendUser(newUser);

                observers.forEach(ICreateObserver::notifyCreate);
            }
        }
    }

    public void retour() {
        observers.forEach(ICreateObserver::notifyRetour);
    }
}
