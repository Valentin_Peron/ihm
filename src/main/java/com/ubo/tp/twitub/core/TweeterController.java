package com.ubo.tp.twitub.core;

import com.ubo.tp.twitub.datamodel.Twit;
import com.ubo.tp.twitub.datamodel.User;
import com.ubo.tp.twitub.observer.ITweeterObserver;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class TweeterController {

    protected final Set<ITweeterObserver> observers;

    protected EntityManager entityManager;

    protected User user;

    public TweeterController(EntityManager entityManager, User user) {
        observers = new HashSet<>();
        this.entityManager = entityManager;
        this.user = user;
    }

    public void addObserver(ITweeterObserver observer) {
        observers.add(observer);
    }

    public void removeObserver(ITweeterObserver observer) {
        observers.remove(observer);
    }

    public void tweet(String text) {
        Twit newTwit = new Twit(UUID.randomUUID(), user,  System.currentTimeMillis(), text);

        entityManager.sendTwit(newTwit);

        observers.forEach(observer -> observer.notifyTwit(newTwit));
    }
}
