package com.ubo.tp.twitub.core;

import com.ubo.tp.twitub.datamodel.IDatabase;
import com.ubo.tp.twitub.datamodel.User;
import com.ubo.tp.twitub.observer.ILoginObserver;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class LoginController {

    protected final Set<ILoginObserver> observers;

    protected IDatabase database;

    public LoginController(IDatabase database) {
        observers = new HashSet<>();
        this.database = database;
    }

    public void addObserver(ILoginObserver observer) {
        observers.add(observer);
    }

    public void removeObserver(ILoginObserver observer) {
        observers.remove(observer);
    }

    public void login(String tag, String password) {
        Set<User> users = database.getUsers();

        Optional<User> result = users.stream().filter(user -> user.getUserTag().equals(tag) && user.getUserPassword().equals(password)).findFirst();

        if (result.isPresent()) {
            observers.forEach(observer -> observer.notifyLogin(result.get()));
        } else {
            observers.forEach(ILoginObserver::notifyNotLogin);
        }
    }

    public void create() {
        observers.forEach(ILoginObserver::notifyShowCreate);
    }
}
