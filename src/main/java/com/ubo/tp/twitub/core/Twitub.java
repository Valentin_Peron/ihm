package com.ubo.tp.twitub.core;

import com.ubo.tp.twitub.datamodel.Database;
import com.ubo.tp.twitub.datamodel.IDatabase;
import com.ubo.tp.twitub.datamodel.Twit;
import com.ubo.tp.twitub.datamodel.User;
import com.ubo.tp.twitub.events.file.IWatchableDirectory;
import com.ubo.tp.twitub.events.file.WatchableDirectory;
import com.ubo.tp.twitub.ihm.*;
import com.ubo.tp.twitub.observer.*;

import javax.swing.*;
import java.io.File;
import java.util.List;
import java.util.Set;

/**
 * Classe principale l'application.
 * 
 * @author S.Lucas
 */
public class Twitub implements ILoginObserver, ICreateObserver, ITweeterObserver, IRechercherUserObserver, IProfileObserver, IRechercherTwitObserver {
	/**
	 * Base de données.
	 */
	protected IDatabase mDatabase;

	/**
	 * Gestionnaire des entités contenu de la base de données.
	 */
	protected EntityManager mEntityManager;

	/**
	 * Vue principale de l'application.
	 */
	protected TwitubMainView mMainView;

	/**
	 * User connecté
	 */
	protected User user;

	/**
	 * Console
	 */
	protected Console console;

	/**
	 * Classe de surveillance de répertoire
	 */
	protected IWatchableDirectory mWatchableDirectory;

	/**
	 * Répertoire d'échange de l'application.
	 */
	protected String mExchangeDirectoryPath;

	/**
	 * Idnique si le mode bouchoné est activé.
	 */
	protected boolean mIsMockEnabled = false;

	/**
	 * Nom de la classe de l'UI.
	 */
	protected String mUiClassName;

	/**
	 * Constructeur.
	 */
	public Twitub() {
		// Initialisation de la console
		this.initConsole();

		// Initialisation de la base de données
		this.initDatabase();

		if (this.mIsMockEnabled) {
			// Initialisation du bouchon de travail
			this.initMock();
		}

		//Initialisation du style
		this.initLookAndFeel();

		// Initialisation de l'IHM
		this.initGui();

		// Initialisation du répertoire d'échange
		this.initDirectory();
	}

	/**
	 * Initialisation du look and feel de l'application.
	 */
	protected void initLookAndFeel() {

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Initialisation de l'interface graphique.
	 */
	protected void initGui() {
		mMainView = new TwitubMainView(this);
	}

	protected void initConsole() { console = new Console(); }

	/**
	 * Initialisation du répertoire d'échange (depuis la conf ou depuis un file
	 * chooser). <br/>
	 * <b>Le chemin doit obligatoirement avoir été saisi et être valide avant de
	 * pouvoir utiliser l'application</b>
	 */
	protected void initDirectory() {
		File folder = mMainView.getRepertoire();

		if (!isValideExchangeDirectory(folder)) {
			System.exit(0);
		}

		initDirectory(folder.getAbsolutePath());
	}

	/**
	 * Indique si le fichier donné est valide pour servire de répertoire
	 * d'échange
	 * 
	 * @param directory
	 *            , Répertoire à tester.
	 */
	protected boolean isValideExchangeDirectory(File directory) {
		// Valide si répertoire disponible en lecture et écriture
		return directory != null && directory.exists() && directory.isDirectory() && directory.canRead()
				&& directory.canWrite();
	}

	/**
	 * Initialisation du mode bouchoné de l'application
	 */
	protected void initMock() {
		TwitubMock mock = new TwitubMock(this.mDatabase, this.mEntityManager);
		mock.showGUI();
	}

	/**
	 * Initialisation de la base de données
	 */
	protected void initDatabase() {
		mDatabase = new Database();
		mDatabase.addObserver(console);
		mEntityManager = new EntityManager(mDatabase);
	}

	/**
	 * Initialisation du répertoire d'échange.
	 * 
	 * @param directoryPath
	 */
	public void initDirectory(String directoryPath) {
		mExchangeDirectoryPath = directoryPath;
		mWatchableDirectory = new WatchableDirectory(directoryPath);
		mEntityManager.setExchangeDirectory(directoryPath);

		mWatchableDirectory.initWatching();
		mWatchableDirectory.addObserver(mEntityManager);
	}

	public void show() {
		showLogin(false);
		mMainView.setVisible();
	}

	public void showLogin(boolean isError) {
		LoginController loginController = new LoginController(mDatabase);
		loginController.addObserver(this);

		mMainView.showLogin(new LoginView(loginController, isError));
	}

	public void showCreate(String erreur) {
		CreateController createController = new CreateController(mDatabase, mEntityManager);
		createController.addObserver(this);

		mMainView.showCreate(new CreateView(createController, erreur));
	}

	public void showProfile(User otherUser) {
		ProfileController profileController = new ProfileController(mDatabase, mEntityManager, user);
		profileController.addObserver(this);

		mMainView.showProfile(new ProfileView(profileController, user, otherUser, mDatabase.getFollowed(user)));
	}

	public void showRechercherUser(List<User> users) {
		RechercherUserController rechercherUserController = new RechercherUserController(mDatabase);
		rechercherUserController.addObserver(this);

		mMainView.showRechercherUser(new RechercherUserView(rechercherUserController, users));
	}

	public void showTweeter() {
		TweeterController tweeterController = new TweeterController(mEntityManager, user);
		tweeterController.addObserver(this);

		mMainView.showTweeter(new TweeterView(tweeterController));
	}

	public void showRechercherTwit(Set<Twit> twits) {
		RechercherTwitController rechercherTwitController = new RechercherTwitController(mDatabase);
		rechercherTwitController.addObserver(this);

		mMainView.showRechercherTwit(new RechercherTwitView(rechercherTwitController, twits));
	}

	public void deconnexion() {
		this.user = null;
		showLogin(false);
	}

	@Override
	public void notifyLogin(User user) {
		this.user = user;
		mMainView.initNavTwit(user);
		showProfile(user);
	}

	@Override
	public void notifyNotLogin() {
		showLogin(true);
	}

	@Override
	public void notifyShowCreate() {
		showCreate("");
	}

	@Override
	public void notifyCreate() {
		showLogin(false);
	}

	@Override
	public void notifyNotCreate(String erreur) {
		showCreate(erreur);
	}

	@Override
	public void notifyRetour() {
		showLogin(false);
	}

	@Override
	public void notifyTwit(Twit twit) {
		showTweeter();
	}

	@Override
	public void notifyRechercherUser(List<User> users) {
		showRechercherUser(users);
	}

	@Override
	public void notifyConsulter(User user) {
		showProfile(user);
	}

	@Override
	public void notifyFollow(User user) {
		showProfile(user);
	}

	@Override
	public void notifyRechercherTwit(Set<Twit> twits) {
		showRechercherTwit(twits);
	}
}
