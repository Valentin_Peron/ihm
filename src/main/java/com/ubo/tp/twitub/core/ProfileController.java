package com.ubo.tp.twitub.core;

import com.ubo.tp.twitub.datamodel.IDatabase;
import com.ubo.tp.twitub.datamodel.User;
import com.ubo.tp.twitub.observer.IProfileObserver;

import java.util.HashSet;
import java.util.Set;

public class ProfileController {

    protected final Set<IProfileObserver> observers;

    protected IDatabase database;

    protected EntityManager entityManager;

    protected User user;

    public ProfileController(IDatabase database, EntityManager entityManager, User user) {
        observers = new HashSet<>();
        this.database = database;
        this.entityManager = entityManager;
        this.user = user;
    }

    public void addObserver(IProfileObserver observer) {
        observers.add(observer);
    }

    public void removeObserver(IProfileObserver observer) {
        observers.remove(observer);
    }

    public void follow(User otherUser) {
        Set<String> usersFollowed = user.getFollows();

        if (usersFollowed.stream().filter(follow -> follow.equals(otherUser.getUserTag())).count() != 0) {
            user.removeFollowing(otherUser.getUserTag());
        } else {
            user.addFollowing(otherUser.getUserTag());
        }

        entityManager.sendUser(user);
        observers.forEach(observer -> observer.notifyFollow(otherUser));
    }

    public int getNbFollower(User otherUser) {
        return database.getFollowersCount(otherUser);
    }
}
