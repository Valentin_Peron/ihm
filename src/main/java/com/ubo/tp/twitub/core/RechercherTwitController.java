package com.ubo.tp.twitub.core;

import com.ubo.tp.twitub.datamodel.IDatabase;
import com.ubo.tp.twitub.datamodel.Twit;
import com.ubo.tp.twitub.observer.IRechercherTwitObserver;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class RechercherTwitController {

    protected final Set<IRechercherTwitObserver> observers;

    protected IDatabase database;

    public RechercherTwitController(IDatabase database) {
        observers = new HashSet<>();
        this.database = database;
    }

    public void addObserver(IRechercherTwitObserver observer) {
        observers.add(observer);
    }

    public void removeObserver(IRechercherTwitObserver observer) {
        observers.remove(observer);
    }

    public void rechercherTwit(String keyWord) {
        Set<Twit> twits = database.getTwits();

        if (keyWord.isEmpty()) {
            observers.forEach(observer -> observer.notifyRechercherTwit(twits));
            return;
        }

        char prefix = keyWord.charAt(0);

        String regexAll = ".*" + keyWord + ".*";

        Set<Twit> result;

        if (prefix != '@' && prefix != '#') {
            result = twits.stream().filter(twit -> twit.getText().matches(regexAll) || twit.getTwiter().getUserTag().matches(regexAll)).collect(Collectors.toSet());
        } else if (prefix == '@') {
            String newKeyWord = keyWord.substring(1);
            result = twits.stream().filter(twit -> twit.getTwiter().getUserTag().matches(newKeyWord + ".*")  || twit.getText().matches(regexAll)).collect(Collectors.toSet());
        } else {
            result = twits.stream().filter(twit -> twit.getText().matches(regexAll)).collect(Collectors.toSet());
        }

        Set<Twit> finalResult = result;
        observers.forEach(observer -> observer.notifyRechercherTwit(finalResult));
    }
}
