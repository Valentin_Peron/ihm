package com.ubo.tp.twitub.core;

import com.ubo.tp.twitub.datamodel.IDatabase;
import com.ubo.tp.twitub.datamodel.User;
import com.ubo.tp.twitub.observer.IRechercherUserObserver;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class RechercherUserController {

    protected final Set<IRechercherUserObserver> observers;

    protected IDatabase database;

    public RechercherUserController(IDatabase database) {
        observers = new HashSet<>();
        this.database = database;
    }

    public void addObserver(IRechercherUserObserver observer) {
        observers.add(observer);
    }

    public void removeObserver(IRechercherUserObserver observer) {
        observers.remove(observer);
    }

    public void rechercherUser(String userTag) {
        Set<User> users = database.getUsers();

        List<User> result = users.stream().filter(user -> user.getUserTag().matches("^" + userTag + ".*")).collect(Collectors.toList());

        observers.forEach(observer -> observer.notifyRechercherUser(result));
    }

    public void consulterProfil(User user) {
        observers.forEach(observer -> observer.notifyConsulter(user));
    }

}
