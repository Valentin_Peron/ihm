package com.ubo.tp.twitub.observer;

import com.ubo.tp.twitub.datamodel.User;

public interface ILoginObserver {
    void notifyLogin(User user);

    void notifyNotLogin();

    void notifyShowCreate();
}
