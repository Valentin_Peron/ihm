package com.ubo.tp.twitub.observer;

public interface ICreateObserver {
    void notifyCreate();

    void notifyNotCreate(String erreur);

    void notifyRetour();
}
