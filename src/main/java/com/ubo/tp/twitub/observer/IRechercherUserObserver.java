package com.ubo.tp.twitub.observer;

import com.ubo.tp.twitub.datamodel.User;

import java.util.List;

public interface IRechercherUserObserver {
    void notifyRechercherUser(List<User> users);

    void notifyConsulter(User user);
}
