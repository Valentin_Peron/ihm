package com.ubo.tp.twitub.observer;

import com.ubo.tp.twitub.datamodel.User;

public interface IProfileObserver {
    void notifyFollow(User user);
}
