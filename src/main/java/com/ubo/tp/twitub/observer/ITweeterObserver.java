package com.ubo.tp.twitub.observer;

import com.ubo.tp.twitub.datamodel.Twit;

public interface ITweeterObserver {
    void notifyTwit(Twit twit);
}
